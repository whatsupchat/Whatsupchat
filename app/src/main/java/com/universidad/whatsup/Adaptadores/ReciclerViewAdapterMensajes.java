package com.universidad.whatsup.Adaptadores;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.universidad.whatsup.Constantes.Constantes;
import com.universidad.whatsup.Objetos.Mensajes;
import com.universidad.whatsup.Objetos.Usuario;
import com.universidad.whatsup.R;


import java.util.List;


public class ReciclerViewAdapterMensajes extends RecyclerView.Adapter<ReciclerViewAdapterMensajes.ViewHolder>{


//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		private TextView nombre,mensaje;
		private RelativeLayout contenedor;
		private CardView cardView;

		public ViewHolder(View itemView) {
			super(itemView);
			//enalanzando elementos
			nombre=(TextView)itemView.findViewById(R.id.txtusermensaje);
			mensaje=(TextView)itemView.findViewById(R.id.txtmensajemensaje);
			contenedor = (RelativeLayout)itemView.findViewById(R.id.contenedormensajes);
			cardView = (CardView)itemView.findViewById(R.id.cardview);
		}
	}
	//lista de objetos mensajes
	public List<Mensajes> mensajeslista;
	//contexto para algunos metodos
	public Context context;

	//contructor
	public ReciclerViewAdapterMensajes(List<Mensajes> mensajeslista, Context context) {
		this.mensajeslista = mensajeslista;
		this.context = context;
	}

	//inflando la vista
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mensaje_items,parent,false);
		ViewHolder viewHolder = new ViewHolder(view);
		return viewHolder;
	}


	//holder cuminicacando elemntos con la informacion
	@Override
	public void onBindViewHolder(ViewHolder holder, final int position) {
		holder.nombre.setText(mensajeslista.get(position).getNombre());
		holder.mensaje.setText(mensajeslista.get(position).getMensajes());

		if (holder.nombre.getText().toString().equals(Constantes.Usuario.getNick())){
			holder.nombre.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
		}else {
			holder.cardView.setPaddingRelative(10,10,10,10);
		}
	}

	//retorna el tamaño de los elementos
	@Override
	public int getItemCount() {
		return mensajeslista.size();
	}
}
