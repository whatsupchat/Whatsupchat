package com.universidad.whatsup.Adaptadores;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.universidad.whatsup.Activitys.Main2Activity;
import com.universidad.whatsup.Activitys.SalaChat;
import com.universidad.whatsup.Objetos.Contacto;
import com.universidad.whatsup.R;

import org.json.JSONObject;

import java.util.List;

import static com.universidad.whatsup.Constantes.Constantes.Encriptar;
import static com.universidad.whatsup.Constantes.Constantes.Usuario;
import static com.universidad.whatsup.Constantes.Constantes.WEB_SERVICE;



public class ReciclerViewAdapterAmigos extends RecyclerView.Adapter<ReciclerViewAdapterAmigos.ViewHolder>  {


//clase viewholder
	public static class ViewHolder extends RecyclerView.ViewHolder{
		private TextView nombre,numero,email;
		private LinearLayout contenedoramigo;

		public ViewHolder(View itemView) {
			super(itemView);
			nombre=(TextView)itemView.findViewById(R.id.txtnombreamigo);
			numero=(TextView)itemView.findViewById(R.id.txtnumeroamigo);
			email=(TextView)itemView.findViewById(R.id.txtcorreoamigo);
			contenedoramigo = (LinearLayout)itemView.findViewById(R.id.contenedoramigo);
		}
	}
	public List<Contacto> amigolista;
	final public Context context;
	public ReciclerViewAdapterAmigos(List<Contacto> amigolista, Context context) {
		this.amigolista = amigolista;
		this.context = context;
	}
//inflando la vista
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.amigo_items,parent,false);
		ViewHolder viewHolder = new ViewHolder(view);
		return viewHolder;
	}
//relacionando la informacion con la vista
	@Override
	public void onBindViewHolder(ViewHolder holder, final int position) {
		holder.nombre.setText(amigolista.get(position).getNick());
		holder.numero.setText(amigolista.get(position).getNumero());
		holder.email.setText(amigolista.get(position).getEmail());

		//accion al dar click
		holder.contenedoramigo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				//crear intent para mandar a sala de chat
				Intent intent = new Intent(context, SalaChat.class);

				//datos del contacto mandados ala sala chat
				intent.putExtra("nombre",amigolista.get(position).getNick());
				intent.putExtra("numero",amigolista.get(position).getNumero());
				context.startActivity(intent);
			}
		});

		//accion al mantener precionando
		holder.contenedoramigo.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View view ) {

				//alrt dialog
				AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context);
				dlgAlert.setTitle("Bloquear");
				dlgAlert.setMessage("Bloquear "+amigolista.get(position).getNick());
				dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						RequestQueue requestQueue= Volley.newRequestQueue(context);
						//web service lista de contactos

						String url =WEB_SERVICE+"?accion=cambiarestado&estado=0&usuario="+Usuario.getIdusuario()+"&email="+Encriptar(amigolista.get(position).getEmail().toString());

						JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
							//si hay respuesta
							@Override
							public void onResponse(JSONObject response) {
								Toast.makeText(context, "Contacto "+amigolista.get(position).getNick() +" Bloqueado", Toast.LENGTH_SHORT).show();
								context.startActivity(new Intent(context,Main2Activity.class));
							}
						}, new Response.ErrorListener() {
							//si no hay respuesta
							@Override
							public void onErrorResponse(VolleyError error) {
								Toast.makeText(context, "erro :-( " +error.toString(), Toast.LENGTH_SHORT).show();
							}
						});
						requestQueue.add(jsonRequest);

					}
				});

				//accion cancelar
				dlgAlert.setNegativeButton("Cancelar",null);
				dlgAlert.setCancelable(true);
				dlgAlert.create().show();

				return false;
			}
		});
	}
//retorna tamaño de contactos
	@Override
	public int getItemCount() {
		return amigolista.size();
	}
}
