package com.universidad.whatsup.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.universidad.whatsup.Constantes.Constantes;
import com.universidad.whatsup.R;

import org.json.JSONObject;

import static com.universidad.whatsup.Constantes.Constantes.Desencriptar;
import static com.universidad.whatsup.Constantes.Constantes.Encriptar;
import static com.universidad.whatsup.Constantes.Constantes.WEB_SERVICE;

public class RegistrarseActivity extends AppCompatActivity {

	//Variables Globales
	EditText nick,email,pass,telefono;
	String Snick,Semail,Spass,Stelefono;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registrarse);

		//enlazar variables
		nick = findViewById(R.id.nick);
		email = findViewById(R.id.email);
		pass = findViewById(R.id.pass);
		telefono = findViewById(R.id.telefono);


	}

	public void  Registrar (View view){
		//Guardar valores de los componentes en variables
		Snick = Encriptar(nick.getText().toString().trim());
		Semail= Encriptar(email.getText().toString().trim());
		Spass = Encriptar(pass.getText().toString().trim());
		Stelefono = Encriptar(telefono.getText().toString().trim());

		consultarDatos();


	}

	private void consultarDatos() {
		//Crear un objeto requetqueue
		RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

		//Url del webservice
		String url=WEB_SERVICE+"?accion=validarDatos&email="+Semail+"&telefono="+Stelefono;


		// crear un objeto requet
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				// si hay respuesta
				Toast.makeText(RegistrarseActivity.this, "Su email o telefono ya se encuentra registrado", Toast.LENGTH_SHORT).show();
			}
		}, new Response.ErrorListener() {
			@Override


			public void onErrorResponse(VolleyError error) {

				//error de respuesta
				RegistrarUsuario();

			}
		});

		//añadir el objeto requet al requetqueue
		requestQueue.add(jsonObjectRequest);


	}

	private void RegistrarUsuario()
	{


		if (Snick.isEmpty()||Semail.isEmpty()||Spass.isEmpty()||Stelefono.isEmpty()){
			Toast.makeText(this, "Favor de llenar todos los campos", Toast.LENGTH_SHORT).show();
		}else {

			//Crear un objeto requetqueue
			RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

			//Url del webservice
			String url=WEB_SERVICE+"?accion=registrar&nick="+Snick+"&email="+Semail+"&pass="+Spass+"&telefono="+Stelefono;

			// crear un objeto requet
			JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					// si hay respuesta

					Toast.makeText(getApplicationContext(), "Registrado Correctamente ", Toast.LENGTH_SHORT).show();
					//limpiar campos
					limpiarCampos();

					// inicciar pantalla principal
					startActivity(new Intent(RegistrarseActivity.this,Loguin.class));
					//finalizar actividad
					RegistrarseActivity.this.finish();
				}
			}, new Response.ErrorListener() {
				@Override


				public void onErrorResponse(VolleyError error) {

					//error de respuesta
					Toast.makeText(getApplicationContext(), "Error al registrarse  "+error, Toast.LENGTH_SHORT).show();

				}
			});

			//añadir el objeto requet al requetqueue
			requestQueue.add(jsonObjectRequest);


		}
	}

	private void limpiarCampos() {

		//Limpiar cajas de texto
		nick.setText(null);
		email.setText(null);
		pass.setText(null);
		telefono.setText(null);
	}
}

