package com.universidad.whatsup.Activitys;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.universidad.whatsup.Adaptadores.ReciclerViewAdapterMensajes;
import com.universidad.whatsup.Objetos.Mensajes;
import com.universidad.whatsup.R;


import java.util.ArrayList;
import java.util.List;

import static com.universidad.whatsup.Constantes.Constantes.Desencriptar;
import static com.universidad.whatsup.Constantes.Constantes.Encriptar;
import static com.universidad.whatsup.Constantes.Constantes.Usuario;

public class SalaChat extends AppCompatActivity {


	String nombre=null,numero=null;

	EditText mensaje ;
	FloatingActionButton button;

	private RecyclerView recyclerView;
	private ReciclerViewAdapterMensajes adapter;
	List<Mensajes> MENSAJE = new ArrayList<>() ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sala_chat);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mensaje=findViewById(R.id.txtchat);
		button = findViewById(R.id.floatingActionButton);
		//recicler view
		recyclerView = findViewById(R.id.reciclermensajes);


		// traer datos de contactos
		nombre = getIntent().getExtras().getString("nombre");
		numero = getIntent().getExtras().getString("numero");

		adapter = new ReciclerViewAdapterMensajes(MENSAJE,SalaChat.this);

		//Crear Id Salda de Chat
		Double nu =Double.parseDouble(numero)*Double.parseDouble(Usuario.getTelefono());
		String s = Double.toHexString(nu);
		s=s.substring(4);



		adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
			@Override
			public void onItemRangeInserted(int positionStart, int itemCount) {
				super.onItemRangeInserted(positionStart, itemCount);
				recyclerView.scrollToPosition(adapter.getItemCount()-1);
			}
		});


		getSupportActionBar().setTitle(nombre);

		//firebase Database
		final FirebaseDatabase database = FirebaseDatabase.getInstance();

		//Referencia a salada de chat
		final DatabaseReference  myRef = database.getReference(s);

		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (mensaje.getText().toString().isEmpty()){
					//validacion de mensajes vacios
					Toast.makeText(SalaChat.this, "mensaje vacio", Toast.LENGTH_SHORT).show();
				}else {
					try {
						//mandar datos a la Data de firebase
						myRef.push().setValue(new Mensajes(Encriptar(Usuario.getNick()), Encriptar(mensaje.getText().toString())));
						//limpiar campo mensaje
						mensaje.setText(null);
					} catch (Exception e) {
						Log.i("error","Mensajes a data firebase");
					}
				}

			}
		});


//Vento cuando hay cambio en la base de datos
		myRef.addChildEventListener(new ChildEventListener() {
			@Override

			// metodo que se ejecuta cuando detecta un cambio en la base de datos
			public void onChildAdded(DataSnapshot dataSnapshot, String s) {
				LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, true);
				linearLayoutManager.setReverseLayout(false);
				linearLayoutManager.setStackFromEnd(true);
				//mover al ultimo mensaje
				linearLayoutManager.scrollToPosition(adapter.getItemCount()-1);
				try {
					//enblazando objeto Mensaje
					Mensajes m = dataSnapshot.getValue(Mensajes.class);
					//Mandar mensaje al recicler view
					MENSAJE.add(new Mensajes(Desencriptar(m.getNombre()),Desencriptar(m.getMensajes())));
					//mandar adaptador de mensajes
					recyclerView.setAdapter(adapter);
					//asiganar layout al recicler view
					recyclerView.setLayoutManager(linearLayoutManager);
					//posicionar el scrroll en el ultimo elemento
					recyclerView.smoothScrollToPosition(adapter.getItemCount()-1);

				}catch (Exception e){
					//error
					Log.i("erro","cambios de la database realtime");

				}
			}


			@Override
			public void onChildChanged(DataSnapshot dataSnapshot, String s) {

			}

			@Override
			public void onChildRemoved(DataSnapshot dataSnapshot) {

			}

			@Override
			public void onChildMoved(DataSnapshot dataSnapshot, String s) {

			}

			@Override
			public void onCancelled(DatabaseError databaseError) {

			}
		});

	}



}
