package com.universidad.whatsup.Activitys;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.universidad.whatsup.Constantes.Constantes;
import com.universidad.whatsup.Datos.Conexion;
import com.universidad.whatsup.R;

import org.json.JSONArray;
import org.json.JSONObject;


import static com.universidad.whatsup.Constantes.Constantes.Desencriptar;
import static com.universidad.whatsup.Constantes.Constantes.Encriptar;
import static com.universidad.whatsup.Constantes.Constantes.Usuario;
import static com.universidad.whatsup.Constantes.Constantes.WEB_SERVICE;


public class Loguin extends AppCompatActivity {

	Button login;
	TextView registrarse;
	EditText email,pass;
	Conexion conn = null;
	SQLiteDatabase db = null;
	CheckBox sesion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		login = findViewById(R.id.login);
		registrarse = findViewById(R.id.txtregistrarse);

		email = findViewById(R.id.nick);
		pass=findViewById(R.id.pass);

		sesion = findViewById(R.id.sesion);

		revisarSesion();

		login.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {



				//Crear un objeto requetqueue
				RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
				String Semail = Encriptar(email.getText().toString().trim());
				String Spass = Encriptar(pass.getText().toString().trim());

				if (Semail.isEmpty()||Spass.isEmpty()){
					Toast.makeText(Loguin.this, "Uno de los campos esta vacio", Toast.LENGTH_SHORT).show();
				}else {

				//Url del webservice
				String url=WEB_SERVICE+"?accion=login&email="+Semail+"&pass="+Spass;

				// crear un objeto requet
				JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						// si hay respuesta


						try {
							JSONArray json=response.optJSONArray("usuario");
							JSONObject jsonObject= null;
							jsonObject=json.getJSONObject(0);
							Usuario.setNick(Desencriptar(jsonObject.optString("nick")));
							Usuario.setTelefono(Desencriptar(jsonObject.optString("telefono")));
							Usuario.setIdusuario(jsonObject.optString("id"));

							if (sesion.isChecked()){
								conn = new Conexion(getApplicationContext(),"usuario",null,1);
								db=conn.getWritableDatabase();
								db.execSQL("insert into usuario (id,nick,telefono,sesion) values ('"+Usuario.getIdusuario()+"','"+Usuario.getNick()+"','"+Usuario.getTelefono()+"','1');");
								db.close();

							}else {
								Log.i("seleccionado","no seleccionado");
							}


							Toast.makeText(Loguin.this, "Bienvenido "+Usuario.getNick(), Toast.LENGTH_SHORT).show();
						}catch (Exception e){
							Toast.makeText(Loguin.this, ""+e, Toast.LENGTH_SHORT).show();
						}
						Toast.makeText(getApplicationContext(), "Sesion Iniciada", Toast.LENGTH_SHORT).show();

						//iniciar activity
						startActivity(new Intent(Loguin.this,Main2Activity.class));

						Loguin.this.finish();


					}
				}, new Response.ErrorListener() {
					@Override


					public void onErrorResponse(VolleyError error) {

						//error de respuesta
						Toast.makeText(getApplicationContext(), "Datos Incorrectos", Toast.LENGTH_SHORT).show();

					}
				});

				//añadir el objeto requet al requetqueue
				requestQueue.add(jsonObjectRequest);



			}
			}
		});
		registrarse.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(Loguin.this,RegistrarseActivity.class));
				Loguin.this.finish();
			}
		});
	}

	private void revisarSesion() {



		try {
			Conexion conn = new Conexion(this, "usuario", null, 1);
			SQLiteDatabase db = conn.getWritableDatabase();
			Cursor cursor = db.rawQuery("select id,nick,telefono,sesion from  usuario where sesion='1';",null);
			cursor.moveToFirst();
			if (cursor.getString(3).equals("1"))
			{
				Usuario.setIdusuario(cursor.getString(0));
				Usuario.setNick(cursor.getString(1));
				Usuario.setTelefono(cursor.getString(2));
				startActivity(new Intent(getApplicationContext(),Main2Activity.class));
				finish();


			}else {

			}

		}catch (Exception e){
			Log.e("Revisasr sesión",e.toString());
		}


	}
}
