package com.universidad.whatsup.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.universidad.whatsup.Activitys.Loguin;
import com.universidad.whatsup.Activitys.Main2Activity;
import com.universidad.whatsup.R;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.universidad.whatsup.Constantes.Constantes.Desencriptar;
import static com.universidad.whatsup.Constantes.Constantes.Encriptar;
import static com.universidad.whatsup.Constantes.Constantes.Usuario;
import static com.universidad.whatsup.Constantes.Constantes.WEB_SERVICE;

public class AddContacto extends AppCompatActivity {

	Button buscar,confirmar,cancelar;
	TextView numero,nick,email;
	EditText txtnumero;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_contacto);

		// Enlazar Botones
		buscar = findViewById(R.id.btnbuscar);
		confirmar = findViewById(R.id.btnconfirmar);
		cancelar = findViewById(R.id.btncancelar);
		//Enlazar Texview
		nick = findViewById(R.id.usuarionick);
		numero = findViewById(R.id.usuarionumero);
		email = findViewById(R.id.usuarioemail);
		// Enlazar Edittext
		txtnumero = findViewById(R.id.txtnumero);

		// accion boton buscar

		buscar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				consultarUsuario();
			}
		});
		// accion boton cancelar
		cancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				startActivity(new Intent(AddContacto.this,Main2Activity.class));

			}
		});

		//accion boton Confrimar
		confirmar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				insertarContacto();
				startActivity(new Intent(AddContacto.this,Main2Activity.class));
				finish();
			}
		});
	}

	private void insertarContacto() {
		RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

		String url=WEB_SERVICE+"?accion=insertarcontacto&nick="+Encriptar(nick.getText().toString().trim().replace(" ","%20"))+"&email="+Encriptar(email.getText().toString().trim())+"&numero="+Encriptar(numero.getText().toString().trim())+"&usuario="+Usuario.getIdusuario();

		//http://chatseguro.es/chat/chat.php?accion=


		// crear un objeto requet
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				// si hay respuesta

				try {


				}catch (Exception e){
					Toast.makeText(AddContacto.this, "tiempo de espera agotado"+e, Toast.LENGTH_SHORT).show();
				}




			}
		}, new Response.ErrorListener() {
			@Override


			public void onErrorResponse(VolleyError error) {

				//error de respuesta
				Toast.makeText(AddContacto.this, "error al añadir contacto "+error, Toast.LENGTH_SHORT).show();

			}
		});

		//añadir el objeto requet al requetqueue
		requestQueue.add(jsonObjectRequest);


	}

	private void consultarUsuario() {
		RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());

		String url=WEB_SERVICE+"?accion=adduser&telefono="+Encriptar(txtnumero.getText().toString().trim());


		// crear un objeto requet
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				// si hay respuesta

				try {
					JSONArray json=response.optJSONArray("contactos");
					JSONObject jsonObject= null;
					jsonObject=json.getJSONObject(0);
					nick.setText(Desencriptar(jsonObject.optString("nick")));
					numero.setText(Desencriptar(jsonObject.optString("telefono")));
					email.setText(Desencriptar(jsonObject.optString("email")));



				}catch (Exception e){
					Toast.makeText(AddContacto.this, "error"+e, Toast.LENGTH_SHORT).show();
				}




			}
		}, new Response.ErrorListener() {
			@Override


			public void onErrorResponse(VolleyError error) {

				//error de respuesta
				nick.setText("No hay datos");
				numero.setText("No hay datos");
				email.setText("No hay datos");

			}
		});

		//añadir el objeto requet al requetqueue
		requestQueue.add(jsonObjectRequest);


	}
}
