package com.universidad.whatsup.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.universidad.whatsup.Adaptadores.ReciclerViewAdapterAmigos;
import com.universidad.whatsup.Adaptadores.ReciclerViewAdapterBloqueados;
import com.universidad.whatsup.Constantes.Constantes;
import com.universidad.whatsup.Objetos.Contacto;
import com.universidad.whatsup.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.universidad.whatsup.Constantes.Constantes.Desencriptar;


public class Bloqueados extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener {

	View  view;

	private RecyclerView recyclerView;
	private ReciclerViewAdapterBloqueados adapter;

	List<Contacto> CONTACTO = new ArrayList<>() ;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_bloqueados, container, false);

		// enlazando componentes
		recyclerView = view.findViewById(R.id.listabloqueados);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		obtenerBloqueados();
		return  view;
	}
	public void obtenerBloqueados() {

		// web services bloquedos

		RequestQueue requestQueue= Volley.newRequestQueue(getContext());


		String url ="http://chatseguro.es/chat/chat.php?accion=listarbloqueados&usuario="+ Constantes.Usuario.getIdusuario();

		JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null,this, this);
		requestQueue.add(jsonRequest);

	}

	@Override
	public void onErrorResponse(VolleyError error) {

	}

	@Override
	public void onResponse(JSONObject response) {
		JSONObject jsonObject = null;
		JSONArray json  = response.optJSONArray("contactos");




		try {
			//traer datos y desmciptralos
			for (int i =0 ; i<json.length();i++){
				jsonObject = json.getJSONObject(i);
				String Nick = Desencriptar(jsonObject.optString("nick"));
				String Email = Desencriptar(jsonObject.optString("email"));
				String Telefono = Desencriptar(jsonObject.optString("numero"));
				CONTACTO.add(new Contacto(Nick.replace("%20"," "),Telefono,Email));

				adapter = new ReciclerViewAdapterBloqueados(CONTACTO,getContext());
				recyclerView.setAdapter(adapter);


			}


		}catch (Exception e ){
			Toast.makeText(getContext(), "Servidor Innacesible", Toast.LENGTH_SHORT).show();
		}


	}
}
