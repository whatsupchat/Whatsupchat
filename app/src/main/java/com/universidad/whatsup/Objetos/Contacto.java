package com.universidad.whatsup.Objetos;



public class Contacto {

	private String nick;
	private String numero;
	private String email;

	public Contacto() {
	}

	public Contacto(String nick, String numero, String email) {
		this.nick = nick;
		this.numero = numero;
		this.email= email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}
