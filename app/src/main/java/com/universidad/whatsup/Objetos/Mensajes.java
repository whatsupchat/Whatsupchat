package com.universidad.whatsup.Objetos;



public class Mensajes {

	private String nombre ;
	private  String mensajes;

	public Mensajes() {
	}

	public Mensajes(String nombre, String mensajes) {
		this.nombre = nombre;
		this.mensajes = mensajes;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMensajes() {
		return mensajes;
	}

	public void setMensajes(String mensajes) {
		this.mensajes = mensajes;
	}
}
