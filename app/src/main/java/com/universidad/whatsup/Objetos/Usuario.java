package com.universidad.whatsup.Objetos;

/**
 * Created by machi on 13/04/2018.
 */

public class Usuario {
	String idusuario;
	String nick;
	String email;
	String telefono;

	public Usuario() {
	}

	public Usuario(String idusuario, String nick, String email, String telefono) {
		this.idusuario = idusuario;
		this.nick = nick;
		this.email = email;
		this.telefono = telefono;
	}

	public String getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(String idusuario) {
		this.idusuario = idusuario;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
