package com.universidad.whatsup.Constantes;


import com.universidad.whatsup.Objetos.Usuario;

public class Constantes {

	public static String WEB_SERVICE = "http://chatseguro.es/chat/chat.php";
	public static Usuario Usuario = new Usuario();
	public static String  CREAR_TABLA_USUARIO="CREATE TABLE `usuario` ( `id` TEXT NOT NULL, `nick` TEXT, `telefono` TEXT, `sesion` INTEGER, PRIMARY KEY(`id`) )";

	public static String Encriptar(String s) {
		char array [] = s.toCharArray();

		for (int i = 0; i < array.length; i++)
		{
			array[i] = (char)(array[i]*(char)+5);
		}
		return String.valueOf(array);
	}
	public static String Desencriptar(String s) {
		char array [] = s.toCharArray();

		for (int i = 0; i < array.length; i++)
		{
			array[i] = (char)(array[i]/(char)+5);
		}
		return String.valueOf(array);
	}
}
